﻿namespace CloudClient
{
    partial class ChoseAlgorithmForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnXxtea = new System.Windows.Forms.Button();
            this.btnKnapsack = new System.Windows.Forms.Button();
            this.btnSimpleSub = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.tbKey = new System.Windows.Forms.TextBox();
            this.lbInvalidKey = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnXxtea
            // 
            this.btnXxtea.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXxtea.Location = new System.Drawing.Point(54, 137);
            this.btnXxtea.Name = "btnXxtea";
            this.btnXxtea.Size = new System.Drawing.Size(183, 44);
            this.btnXxtea.TabIndex = 0;
            this.btnXxtea.Text = "XXTEA";
            this.btnXxtea.UseVisualStyleBackColor = true;
            this.btnXxtea.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnKnapsack
            // 
            this.btnKnapsack.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnKnapsack.Location = new System.Drawing.Point(54, 187);
            this.btnKnapsack.Name = "btnKnapsack";
            this.btnKnapsack.Size = new System.Drawing.Size(183, 44);
            this.btnKnapsack.TabIndex = 1;
            this.btnKnapsack.Text = "Knapsack";
            this.btnKnapsack.UseVisualStyleBackColor = true;
            this.btnKnapsack.Click += new System.EventHandler(this.btnKnapsack_Click);
            // 
            // btnSimpleSub
            // 
            this.btnSimpleSub.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSimpleSub.Location = new System.Drawing.Point(54, 241);
            this.btnSimpleSub.Name = "btnSimpleSub";
            this.btnSimpleSub.Size = new System.Drawing.Size(183, 44);
            this.btnSimpleSub.TabIndex = 2;
            this.btnSimpleSub.Text = "Simple substitution";
            this.btnSimpleSub.UseVisualStyleBackColor = true;
            this.btnSimpleSub.Click += new System.EventHandler(this.btnSimpleSub_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(-1, 63);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 24);
            this.label1.TabIndex = 3;
            this.label1.Text = "Set key";
            // 
            // tbKey
            // 
            this.tbKey.Location = new System.Drawing.Point(76, 65);
            this.tbKey.Name = "tbKey";
            this.tbKey.Size = new System.Drawing.Size(226, 22);
            this.tbKey.TabIndex = 4;
            // 
            // lbInvalidKey
            // 
            this.lbInvalidKey.AutoSize = true;
            this.lbInvalidKey.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbInvalidKey.ForeColor = System.Drawing.Color.Red;
            this.lbInvalidKey.Location = new System.Drawing.Point(99, 90);
            this.lbInvalidKey.Name = "lbInvalidKey";
            this.lbInvalidKey.Size = new System.Drawing.Size(97, 24);
            this.lbInvalidKey.TabIndex = 5;
            this.lbInvalidKey.Text = "Invalid key";
            this.lbInvalidKey.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(302, 20);
            this.label2.TabIndex = 6;
            this.label2.Text = "Set key and chose encryption algorithm";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // ChoseAlgorithmForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(314, 308);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lbInvalidKey);
            this.Controls.Add(this.tbKey);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnSimpleSub);
            this.Controls.Add(this.btnKnapsack);
            this.Controls.Add(this.btnXxtea);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ChoseAlgorithmForm";
            this.Text = "ChoseAlgorithmForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnXxtea;
        private System.Windows.Forms.Button btnKnapsack;
        private System.Windows.Forms.Button btnSimpleSub;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbKey;
        private System.Windows.Forms.Label lbInvalidKey;
        private System.Windows.Forms.Label label2;
    }
}