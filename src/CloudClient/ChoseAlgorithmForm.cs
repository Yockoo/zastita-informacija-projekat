﻿using CypherLibrary;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CloudClient
{
    public partial class ChoseAlgorithmForm : Form
    {
        public CypherAlgorithm Algorithm { get; set; }
        public string Key { get; set; }
        public ChoseAlgorithmForm()
        {
            InitializeComponent();
        }
        private bool validateInput()
        {
            if (tbKey.Text == null || tbKey.Text == "")
            {
                lbInvalidKey.Visible = true;
                return false;
            }
            else
            {
                Key = tbKey.Text;
                return true;
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            if (!validateInput())
                return;
            this.Algorithm = CypherAlgorithm.XXTEA;
            DialogResult = DialogResult.OK;
            this.Close();

        }

        private void btnKnapsack_Click(object sender, EventArgs e)
        {
            if (!validateInput())
                return;
            this.Algorithm = CypherAlgorithm.Knapsack;
            DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnSimpleSub_Click(object sender, EventArgs e)
        {
            if (!validateInput())
                return;
            this.Algorithm = CypherAlgorithm.SimpleSubstitution;
            DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}
