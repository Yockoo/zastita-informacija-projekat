﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace CloudClient
{
    [Serializable()]
    [XmlRoot("UserFiles")]
    public class MyFileList
    {
        [XmlArray("MyFiles")]
        [XmlArrayItem("File", typeof(MyFileInfo))]
        public List<MyFileInfo> FileList { get; set; }

        public MyFileList() { }
        public void Add(MyFileInfo file)
        {
            if (FileList == null)
                FileList = new List<MyFileInfo>();
            FileList.Add(file);
        }
        public MyFileInfo getFile(string fileName)
        {
            return FileList.Find(el => el.FileName == fileName);
        }
    }
}
