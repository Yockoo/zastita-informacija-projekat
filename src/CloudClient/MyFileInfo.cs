﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudClient
{
    [Serializable]
    public class MyFileInfo
    {
        public string FileName { get; set; }
        public string Key { get; set; }
        public CypherLibrary.CypherAlgorithm Algorithm { get; set; }
        public string HashCode { get; set; }

        public MyFileInfo() { }
        public MyFileInfo(string fileNama, string password,CypherLibrary.CypherAlgorithm alg)
        {
            this.FileName = fileNama;
            this.Algorithm = alg;
            this.Key = password;
        }
        public MyFileInfo(string fileNama, string password, CypherLibrary.CypherAlgorithm alg,string hash)
        {
            this.FileName = fileNama;
            this.Algorithm = alg;
            this.Key = password;
            this.HashCode = hash;
        }
    }
}
