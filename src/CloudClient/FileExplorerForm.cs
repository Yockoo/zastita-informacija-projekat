﻿using CypherLibrary;
using CloudClient.ServiceReference;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;

namespace CloudClient
{
    public partial class FileExplorerForm : Form
    {
       ServiceReference.ICloudService cloud = new ServiceReference.CloudServiceClient();

        private MyFileList _myFiles;
        string _username;
        IList<string> _files;
        private delegate byte[] Cypher(byte[] data, string key);

        public FileExplorerForm(string username)
        {
            this._username = username;
            InitializeComponent();
            lbUsername.Text = username;
            login();
        }
        private void login()
        {
            LoginRequest logReq = new LoginRequest
            {
                Username = _username
            };
            LoginResponse res = new LoginResponse();
            res = cloud.Login(logReq);
            _files = res.FileList;
            lbDiskSize.Text = Convert.ToString(Math.Round(converBytesToMB(res.DiskSize), 3));
            lbDiskSize.Text += " MB";
            lbUsedSpace.Text = Convert.ToString(Math.Round(converBytesToMB(res.AvailableDisk), 3));
            lbUsedSpace.Text += " MB";
            this.refreshFilesView();
        }
        private void refreshFilesView()
        {
            if (_files == null || _files.Count == 0)
            {
                lbNoFiles.Visible = true;
                return;
            }
            lvFiles.Items.Clear();
            foreach (string fileName in _files)
            {
                lvFiles.Visible = true;
                this.lvFiles.Items.Add(fileName);
            }
        }

        private void btnLogOut_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private double converBytesToMB(long bytes)
        {
            return bytes * Math.Pow(10, -6);
        }

       

        private Cypher createEncryptionDelagete(CypherAlgorithm algorithm)
        {
            switch (algorithm)
            {
                case CypherAlgorithm.SimpleSubstitution:
                    return SimpleSubstitution.Ecrypt;
                case CypherAlgorithm.XXTEA:
                    return XXTEA.Ecrypt;
                case CypherAlgorithm.Knapsack:
                    return Knapsack.Encrypt;
                default:
                    return null;
            }
        }
        private Cypher createDencryptionDelagete(CypherAlgorithm algorithm)
        {
            switch (algorithm)
            {
                case CypherAlgorithm.SimpleSubstitution:
                    return SimpleSubstitution.Decrypt;
                case CypherAlgorithm.XXTEA:
                    return XXTEA.Decrypt;
                case CypherAlgorithm.Knapsack:
                    return Knapsack.Decrypt;
                default:
                    return null;
            }
        }

        private void addUploadedFileInfo(string fileName, string password, CypherAlgorithm algorithm, string hashCode)
        {
            if (_myFiles == null)
                loadMyFilesInfo();
            MyFileInfo newFile = new MyFileInfo(fileName, password, algorithm, hashCode);
            _myFiles.Add(newFile);
            saveMyFilesInfo();
        }
        private string getPathToMyFileList()
        {
            StringBuilder sb = new StringBuilder("Users/");
            sb.Append(_username);
            DirectoryInfo dirInfo = new DirectoryInfo(sb.ToString());
            if (!dirInfo.Exists)
                dirInfo.Create();
            sb.Append("/MyFiles.bin");
            return sb.ToString();
        }
        private string getPathToMyFileListXML()
        {
            StringBuilder sb = new StringBuilder("Users/");
            sb.Append(_username);
            DirectoryInfo dirInfo = new DirectoryInfo(sb.ToString());
            if (!dirInfo.Exists)
                dirInfo.Create();
            sb.Append("/MyFiles.xml");
            return sb.ToString();
        }
        private void saveMyFilesXML()
        {
            string path = getPathToMyFileListXML();
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.NewLineOnAttributes = true;
            settings.Indent = true;
            var serializer = new XmlSerializer(typeof(MyFileList));
            using (var xtw = XmlWriter.Create(path, settings))
            {
                serializer.Serialize(xtw, this._myFiles);

                xtw.Close();
            }
        }
        private void getMyFilesFromXML()
        {
            string path = getPathToMyFileListXML();
            if (!File.Exists(path))
                createXmlIfNotExist(path, "UserFiles");
            using (StreamReader reader = new StreamReader(path))
            {
                var serializer = new XmlSerializer(typeof(MyFileList));
                _myFiles = (MyFileList)serializer.Deserialize(reader);

                if (_myFiles == null)
                    _myFiles = new MyFileList();

                reader.Close();
            }

        }
        private void saveMyFilesInfo()
        {
            string path = getPathToMyFileList();
            using (var fileStream = File.OpenWrite(path))
            using (var memoryStream = new MemoryStream())
            {
                var formatter = new BinaryFormatter();
                formatter.Serialize(memoryStream, _myFiles);

                memoryStream.Seek(0, SeekOrigin.Begin);
                var bytes = new byte[memoryStream.Length];
                memoryStream.Read(bytes, 0, (int)memoryStream.Length);
                var encryptedBytes = XXTEA.Ecrypt(bytes, _username);

                fileStream.Write(encryptedBytes, 0, encryptedBytes.Length);

                memoryStream.Close();
                fileStream.Close();
            }

        }
        private void loadMyFilesInfo()
        {
            string path = getPathToMyFileList();
            if (!File.Exists(path))
            {
                using (var fc=File.Create(path))
                {
                    fc.Close();
                }
                _myFiles = new MyFileList();
                return;
            }
            byte[] fileBytes = File.ReadAllBytes(path);
            if (fileBytes == null || fileBytes.Length == 0){
                _myFiles = new MyFileList();
                return;
            }
            byte[] decryptedBytes = XXTEA.Decrypt(fileBytes, _username);
            using (var memoryStream = new MemoryStream(decryptedBytes))
            {
                var formatter = new BinaryFormatter();

                _myFiles = (MyFileList)formatter.Deserialize(memoryStream);
                memoryStream.Close();
            }
        }
        private void createXmlIfNotExist(string path, string element)
        {
            {
                using (var xtw = XmlWriter.Create(path))
                {
                    xtw.WriteStartElement(element);
                    xtw.Close();
                }
            }

        }
        
        private void btnDownload_Click(object sender, EventArgs e)
        {
            if (lvFiles.SelectedItems.Count != 1)
            {
                MessageBox.Show("Please select item in list that you wont to download", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            string fileName = lvFiles.SelectedItems[0].Text;

            // Downlod selected file
            FileTransfer ft;
            FileRequest req = new FileRequest();
            req.FileName = fileName;
            req.Username = _username;
            try
            {
                ft = cloud.Download(req);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error while downloading the file.\n"+ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (ft == null)
            {
                MessageBox.Show("Error while downloading the file.\n", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            
            //Get hashCode and algorithm before uploading file
            MyFileInfo fileInfo = getFile(fileName);

            using (MemoryStream ms = new MemoryStream())
            {
                ft.Data.CopyTo(ms);

                //DENCRYPT
                Cypher Decrypt = createDencryptionDelagete(fileInfo.Algorithm);
                byte[] decryptedBytes = Decrypt(ms.ToArray(), fileInfo.Key);

                //Check hachcodes
                string decryptedHashCode = SHA2.GetHashcode(decryptedBytes);
                if (fileInfo.HashCode.CompareTo(decryptedHashCode) != 0)
                {
                    MessageBox.Show("Hashcodes of the uploaded and downloaded file are not equals. File has been corapted", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                // Chose location where to save downloaded file
                string extension = Path.GetExtension(ft.FileName);
                StringBuilder sb = new StringBuilder(extension);
                sb.Append('|');
                sb.Append(extension);
                SaveFileDialog sfd = new SaveFileDialog
                {
                    Filter = sb.ToString()
                };

                sfd.ShowDialog();
                string path = sfd.FileName;
                if (path == null || path == "")
                    return;

                File.WriteAllBytes(path, decryptedBytes);
                System.Diagnostics.Process.Start(path);
            }

        }
        private void btnUpload_Click(object sender, EventArgs e)
        {

            ChoseAlgorithmForm form = new ChoseAlgorithmForm();
            var result = form.ShowDialog();
            if (result != DialogResult.OK)
                return;
            CypherAlgorithm algorithm = form.Algorithm;
            string key = form.Key;

            OpenFileDialog ofd = new OpenFileDialog();
            ofd.ShowDialog();

            string filePath = ofd.FileName;
            if (filePath == "")
                return;


            string fileName = Path.GetFileName(ofd.SafeFileName);
            byte[] fileBytes = File.ReadAllBytes(filePath);

            //get hashcode
            string fileHashcode = SHA2.GetHashcode(fileBytes);
          
            Cypher Encrypt = createEncryptionDelagete(algorithm);
            byte[] encriptedFileBytes = Encrypt(
                fileBytes, 
                algorithm==CypherAlgorithm.Knapsack ? Knapsack.GetPublicKey(key) : key
                );

            FileTransfer tf = new FileTransfer();
            
            using (Stream data = new MemoryStream(encriptedFileBytes))
            {
                tf.Data = data;
                tf.FileName = fileName;
                tf.Username = _username;
                tf.Hashcode = fileHashcode;
                
                UploadResponse res = cloud.Upload(tf);  
                if (!res.Success)
                {
                    MessageBox.Show(res.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                MessageBox.Show(res.Message, "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                addUploadedFileInfo(fileName, key, algorithm, fileHashcode);
                login();
            }
        }

        private MyFileInfo getFile(string fileName)
        {
            if (_myFiles == null)
                loadMyFilesInfo();
            return _myFiles.getFile(fileName);
        }


    }
}
