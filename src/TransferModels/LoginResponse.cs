﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace TransferModels
{
    [MessageContract]
    public class LoginResponse
    {
        [MessageHeader]
        public long DiskSize { get; set; }

        [MessageHeader]
        public long AvailableDisk { get; set; }

        [MessageBodyMember]
        public IList<string> FileList { get; set; }
    }
}
