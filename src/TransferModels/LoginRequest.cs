﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace TransferModels
{
    [MessageContract]
    public class LoginRequest
    {
        [MessageHeader]
        public string Username { get; set; }
    }
}
