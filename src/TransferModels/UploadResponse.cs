﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace TransferModels
{
    [MessageContract]
    public class UploadResponse
    {
        [MessageHeader]
        public bool Success { get; set; }
        [MessageBodyMember]
        public string Message { get; set; }
    }
}
