﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace TransferModels
{
    [MessageContract]
    public class FileTransfer : IDisposable
    {
        [MessageHeader]
        public string Username { get; set; }

        [MessageHeader]
        public string FileName { get; set; }

        [MessageHeader]
        public string Hashcode { get; set; }


        [MessageBodyMember]
        public Stream Data { get; set; }
        public void Dispose()
        {
            if (Data != null)
            {
                Data.Close();
                Data = null;
            }
        }

    }
}
