﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CloudStore.Models
{
    [Serializable]
    public class UserFile
    {
        public Byte[] Content { get; set; }
        public string HashValue { get; set; }
        public string FileName { get; set; }
        public string ownerId { get; set; }

        public UserFile(){ }
        public UserFile(Byte[] content,string fileName, string hashValue,string username){
            this.Content = content;
            this.HashValue = hashValue;
            this.ownerId = username;
            this.FileName = fileName;
        }

    }
}