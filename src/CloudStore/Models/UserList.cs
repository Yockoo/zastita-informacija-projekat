﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace CloudStore.Models
{
    [Serializable()]
    [XmlRoot("UserList")]
    public class UserList
    {
        [XmlArray("Users")]
        [XmlArrayItem("User", typeof(User))]
        public List<User> Users { get; set; }


        public void Add(User newUser)
        {
            if (Users == null)
                this.Users = new List<User>();
            this.Users.Add(newUser);
        }

        public User findUser(string userId)
        {
            return Users.Find(el => el.Username == userId);
        }

    }
}