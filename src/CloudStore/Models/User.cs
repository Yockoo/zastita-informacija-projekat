﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace CloudStore.Models
{
    [Serializable]
    public class User
    {
        public string Username { get; set; }
        public long Size { get; set; }   // in Bytes

        [XmlArray("UserFiles")]
        [XmlArrayItem("FileName",typeof(string))]
        public List<string> FileList=new List<string>();

      
    }
}