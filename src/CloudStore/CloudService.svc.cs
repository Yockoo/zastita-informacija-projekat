﻿using CloudStore.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Web.Hosting;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using TransferModels;

namespace CloudStore
{
    public class CloudService : ICloudService
    {

        private UserList _users;
        private User _loggedUser;
        private string _pathUsers = System.Web.HttpContext.Current.Server.MapPath("~/App_Data/users.xml");
        private string _pathData = System.Web.HttpContext.Current.Server.MapPath("~/App_Data/");

        private long _userDiskSizeLimit = 100000000; // 100 MB

        public CloudService()
        {
            loadUsers();

        }
        private void loadUsers()
        {
            /// Read users from xml
            using (StreamReader reader = new StreamReader(_pathUsers))
            {
                var serializer = new XmlSerializer(typeof(UserList));
                this._users = (UserList)serializer.Deserialize(reader);

                if (_users == null)
                    _users = new UserList();

                reader.Close();
            }
        }

        LoginResponse ICloudService.Login(LoginRequest req)
        {
            _loggedUser = _users.findUser(req.Username);
            LoginResponse lr = new LoginResponse();
            lr.DiskSize = _userDiskSizeLimit;
            if (_loggedUser == null)
            {
                NewUser(req.Username);
                lr.AvailableDisk = 0;
                lr.FileList = null;
            }
            else
            {
                lr.AvailableDisk = _loggedUser.Size;
                lr.FileList = _loggedUser.FileList;

            }
            return lr;
        }

        private void NewUser(string username)
        {

            User newUser = new User
            {
                Username = username,
                Size = 0
            };
            _users.Add(newUser);
            createDirectory(username);
            saveUsers();
        }

        private void createDirectory(string username)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(_pathData);
            sb.Append(username);
            Directory.CreateDirectory(sb.ToString());
        }

        private DirectoryInfo getUserDirectory(string username)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(_pathData);
            sb.Append(username);
            DirectoryInfo dirInfo = new DirectoryInfo(sb.ToString());
            if (!dirInfo.Exists)
                dirInfo.Create();
            return dirInfo;
        }

        private void saveUsers()
        {
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.NewLineOnAttributes = true;
            settings.Indent = true;
            var serializer = new XmlSerializer(typeof(UserList));
            using (var xtw = XmlWriter.Create(_pathUsers,settings))
            {
                serializer.Serialize(xtw, this._users);

                xtw.Close();
            }
        }

        private bool saveFile(DirectoryInfo dirInfo, UserFile file)
        {
            StringBuilder sb = new StringBuilder(dirInfo.FullName);
            sb.Append('/');
            sb.Append(file.FileName);
            sb.Append(".xml");
            var serializer = new XmlSerializer(typeof(UserFile));

            using (var xtw = XmlWriter.Create(sb.ToString()))
            {
                serializer.Serialize(xtw, file);
                xtw.Close();
                return true;
            }
            
        }

        private UserFile readFile(DirectoryInfo dirInfo, string fileName)
        {
            StringBuilder sb = new StringBuilder(dirInfo.FullName);
            sb.Append('/');
            sb.Append(fileName);
            sb.Append(".xml");
            using (StreamReader reader = new StreamReader(sb.ToString()))
            {
                var serializer = new XmlSerializer(typeof(UserFile));
                UserFile file = (UserFile)serializer.Deserialize(reader);
                reader.Close();
                return file;
            }
        }

        private Byte[] readStream(Stream input)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                input.CopyTo(ms);
                return ms.ToArray();
            }
        }

        public FileTransfer Download(FileRequest req)
        {
            DirectoryInfo dirInfo = getUserDirectory(req.Username);
            UserFile file = readFile(dirInfo, req.FileName);
            if (file == null)
                return null;

            try
            {
                Stream data = new MemoryStream(file.Content);
                FileTransfer t = new FileTransfer();
                t.FileName = file.FileName;
                t.Data = data;
                t.Username = req.Username;
                t.Hashcode = file.HashValue;

                return t;
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                return null;
            }
        }
        public UploadResponse Upload(FileTransfer file)
        {
           
            string fileName = file.FileName;
            string username = file.Username;
            string hashcode = file.Hashcode;

            User user = _users.findUser(username);
            long usersDiskSize = user.Size;
            Byte[] fileBytes = readStream(file.Data);
           
            //Check disk limit
            if (_userDiskSizeLimit < usersDiskSize + fileBytes.LongLength)
            {
                return new UploadResponse
                {
                    Success = false,
                    Message = "Not enough memory in repository to save the file"
                };
            }


            DirectoryInfo dirInfo = getUserDirectory(username);
            UserFile uf = new UserFile(fileBytes, fileName, hashcode, username);

            if(saveFile(dirInfo, uf))
            {
                user.Size += fileBytes.LongLength;
                if(!user.FileList.Contains(fileName))
                    user.FileList.Add(fileName);
                saveUsers();
                return new UploadResponse {
                    Success=true,
                    Message=$"{fileName} Successfuly saved"
                };
            }
            else
            {
                return new UploadResponse
                {
                    Success = false,
                    Message = $"Error while saving {fileName} to disk"
                };
            }
        }
        
    }
}
