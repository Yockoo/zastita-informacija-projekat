﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using TransferModels;

namespace CloudStore
{
    [ServiceContract]
    public interface ICloudService
    {
        [OperationContract]
        LoginResponse Login(LoginRequest req);

        [OperationContract]
        UploadResponse Upload(FileTransfer file);

        [OperationContract]
        FileTransfer Download(FileRequest req);
    }
}
