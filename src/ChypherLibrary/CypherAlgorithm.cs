﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CypherLibrary
{
   
    public enum CypherAlgorithm
    {
        SimpleSubstitution,
        XXTEA,
        Knapsack,
        PCBC,
        SHA2
    }
}
