﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CypherLibrary
{
    public static class XXTEA
    {
        public static byte[] Decrypt(byte[] data, string key)
        {
            if (data.Length == 0)
                return null;

            var v = ToLongs(data);
            while (key.Length < 16) { key += key; }
            var k = ToLongs((new UTF8Encoding()).GetBytes(key.Substring(0, 16)));

            UInt32 n = (UInt32)v.Length,
                   z = v[n - 1],
                   y = v[0],
                   delta = 0x9e3779b9,
                   e,
                   q = (UInt32)(6 + (52 / n)),
                   sum = q * delta,
                   p = 0;

            while (sum != 0)
            {
                e = sum >> 2 & 3;

                for (p = (n - 1); p > 0; p--)
                {
                    z = v[p - 1];
                    y = v[p] -= (z >> 5 ^ y << 2) + (y >> 3 ^ z << 4) ^ (sum ^ y) + (k[p & 3 ^ e] ^ z);
                }

                z = v[n - 1];
                y = v[0] -= (z >> 5 ^ y << 2) + (y >> 3 ^ z << 4) ^ (sum ^ y) + (k[p & 3 ^ e] ^ z);

                sum -= delta;
            }

            int pad = (int)v[v.Length - 1];
            return RemoveTrailingNulls(ToBytes(v),pad);
        }

        public static byte[] Ecrypt(byte[] data, string key)
        {
            if (data.Length == 0)
                return null;

            int padding;
            var v = ToLongsCountPadding(data,out padding);
            UInt32[] t = new UInt32[v.Length + 1];
            Array.Copy(v, t, v.Length);
            t[v.Length] = (uint)padding;
            v = t;
            if (v.Length == 1)
            {
                UInt32[] t1 = new UInt32[2];
                t[0] = v[0];
                t[1] = 0;
                v = t1;
            }

            while (key.Length < 16) { key += key; }
            
            var k = ToLongs((new UTF8Encoding()).GetBytes(key.Substring(0, 16)));

            UInt32 n = (UInt32)v.Length,
               z = v[n - 1],
               y = v[0],
               delta = 0x9e3779b9,
               e,
               rounds = (UInt32)(6 + (52 / n)),
               sum = 0,
               p = 0;

            while (rounds-- > 0)
            {
                sum += delta;
                e = sum >> 2 & 3;

                for (p = 0; p < (n - 1); p++)
                {
                    y = v[(p + 1)];
                    z = v[p] += (z >> 5 ^ y << 2) + (y >> 3 ^ z << 4) ^ (sum ^ y) + (k[p & 3 ^ e] ^ z);
                }

                y = v[0];
                z = v[n - 1] += (z >> 5 ^ y << 2) + (y >> 3 ^ z << 4) ^ (sum ^ y) + (k[p & 3 ^ e] ^ z);
            }

           return ToBytes(v);
        }


        private static UInt32[] ToLongsCountPadding(byte[] s,out int padding)
        {
            padding=0;
            var l = new UInt32[(int)Math.Ceiling(((decimal)s.Length / 4))];

            for (int i = 0; i < l.Length; i++)
            {
                if (i != (l.Length - 1))
                {
                    l[i] = ((s[i * 4])) +
                           ((i * 4 + 1) >= s.Length ? (UInt32)0 << 8 : ((UInt32)s[i * 4 + 1] << 8)) +
                           ((i * 4 + 2) >= s.Length ? (UInt32)0 << 16 : ((UInt32)s[i * 4 + 2] << 16)) +
                           ((i * 4 + 3) >= s.Length ? (UInt32)0 << 24 : ((UInt32)s[i * 4 + 3] << 24));
                }
                else 
                {
                    l[i] = (s[i * 4]);
                    if ((i * 4 + 1) >= s.Length)
                    {
                        l[i] += (UInt32)0 << 8;
                        padding++;
                    }
                    else l[i] += ((UInt32)s[i * 4 + 1] << 8);
                    if ((i * 4 + 2) >= s.Length)
                    {
                        l[i] += (UInt32)0 << 16;
                        padding++;
                    }
                    else l[i] += ((UInt32)s[i * 4 + 2] << 16);
                    if ((i * 4 + 3) >= s.Length)
                    {
                        l[i] += (UInt32)0 << 24 ;
                        padding++;
                    }
                    else l[i] += ((UInt32)s[i * 4 + 3] << 24);
                }
            }
            return l;
        }
        private static UInt32[] ToLongs(byte[] s)
        {
           
            var l = new UInt32[(int)Math.Ceiling(((decimal)s.Length / 4))];

            for (int i = 0; i < l.Length; i++)
            {
                    l[i] = ((s[i * 4])) +
                           ((i * 4 + 1) >= s.Length ? (UInt32)0 << 8 : ((UInt32)s[i * 4 + 1] << 8)) +
                           ((i * 4 + 2) >= s.Length ? (UInt32)0 << 16 : ((UInt32)s[i * 4 + 2] << 16)) +
                           ((i * 4 + 3) >= s.Length ? (UInt32)0 << 24 : ((UInt32)s[i * 4 + 3] << 24));
               
            }
            return l;
        }
        private static byte[] ToBytes(UInt32[] l)
        {
            byte[] b = new byte[l.Length * 4];

            for (Int32 i = 0; i < l.Length; i++)
            {
                b[(i * 4)] = (byte)(l[i] & 0xFF);
                b[(i * 4) + 1] = (byte)(l[i] >> (8 & 0xFF));
                b[(i * 4) + 2] = (byte)(l[i] >> (16 & 0xFF));
                b[(i * 4) + 3] = (byte)(l[i] >> (24 & 0xFF));
            }
            return b;
        }
        private static byte[] RemoveTrailingNulls(byte[] b,int padding)
        {
            if (padding == 0)
            {
                byte[] r1 = new byte[b.Length  - 4];
                Array.Copy(b, r1, b.Length  - 4);
                return r1;
            }
            byte[] r = new byte[b.Length - padding-4];
            Array.Copy(b, r, b.Length - padding-4);
            return r;
        }
    }
}
