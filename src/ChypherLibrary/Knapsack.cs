﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CypherLibrary
{
    public static class Knapsack
    {
        class PrivateKey
        {
            public int[] Key { get; set; }
            public int M { get; set; }
            public int N { get; set; }
        }

        public const int KEY_ARRAY_LENGHT = 8;
        public static string GetPublicKey(string key)
        {
            PrivateKey privateKey = getPrivateKey(key);
            int[] publicKey = new int[KEY_ARRAY_LENGHT];
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < KEY_ARRAY_LENGHT; i++)
            {
                publicKey[i] = (privateKey.Key[i] * privateKey.M) % privateKey.N;
                sb.Append((privateKey.Key[i] * privateKey.M) % privateKey.N);
                if (i < KEY_ARRAY_LENGHT - 1)
                    sb.Append('-');
            }
            return sb.ToString();
        }
        private static PrivateKey getPrivateKey(string key)
        {
            Random rand = new Random(key.GetHashCode());
            int sum = 0;
            int[] privateKey = new int[KEY_ARRAY_LENGHT];
            for (int i = 0; i < KEY_ARRAY_LENGHT; i++)
            {
                privateKey[i] = sum + rand.Next(1, 9);
                sum += privateKey[i];
            }
            PrivateKey prKey = new PrivateKey();
            prKey.Key = privateKey;
            prKey.N = sum + rand.Next(1, 10);
            prKey.M = getCoPrimeNumber(prKey.N);
            return prKey;
        }

        public static byte[] Encrypt(byte[] data,string pubKey)
        {
            if (data == null)
                return null;

            string[] tmp = pubKey.Split('-');
            int[] publicKey = Array.ConvertAll(tmp, int.Parse);

            if (publicKey.Length != KEY_ARRAY_LENGHT)
                return null;

            StringBuilder sb = new StringBuilder("");
            for (int i = 0; i < data.Length; i++)
            {
                string dataBinary = Convert.ToString(data[i], 2).PadLeft(8, '0');
                int C = 0;
                for (int j = 0; j < KEY_ARRAY_LENGHT; j++)
                {
                    if (dataBinary[j] == '1')
                        C += Convert.ToInt32(publicKey[j]);
                }
                sb.Append(C);
                if(i!=data.Length-1)
                    sb.Append(" ");
            }
            return Encoding.Unicode.GetBytes(sb.ToString());
        }
        public static byte[] Decrypt(byte[] data, string key)
        {
            if (data == null)
                return null;

            string encrypted = Encoding.Unicode.GetString(data);
            string[] byteCodes = encrypted.Split(' ');

            PrivateKey privateKey = getPrivateKey(key);
            int im = FindIM(privateKey.N, privateKey.M);
            List<byte> DecryptedBytes = new List<byte>();

            foreach (string code in byteCodes)
            {
                int C = Convert.ToInt32(code);
                int TC = (C * im) % privateKey.N;
                string TCBinary = FromDecimalToBinary(TC, privateKey.Key);
                byte M = Convert.ToByte(TCBinary,2);
                DecryptedBytes.Add(M);
            }
            return DecryptedBytes.ToArray();
        }
        private static string FromDecimalToBinary(int number,int[] key)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = KEY_ARRAY_LENGHT-1; i >= 0; i--)
            {
                int val = key[i];
                if (number >= val)
                {
                    sb.Insert(0, "1");
                    number -= val;
                }
                else
                {
                    sb.Insert(0, "0");
                }
            }
            return sb.ToString();
        } 
        private static int getCoPrimeNumber(int number)
        {
            Random rand = new Random(number);
            int coprimeNum = rand.Next(100);
            while (greaterCommonDivider(number,coprimeNum)!=1)
            {
                coprimeNum++;
            }
            return coprimeNum;
        }
        private static int greaterCommonDivider(int a, int b)
        {
            if (a == 0 || b == 0)
                return 0;

            if (a == b)
                return a;

            if (a > b)
                return greaterCommonDivider(a - b, b);

            return greaterCommonDivider(a, b - a);
        }

        private static int FindIM(int n, int m)
        {
            int val =0;
            int res;
            while (true)
            {
                val++;
                res = (val * m) % n;
                if (res == 1)
                    return val;
            }
        }
    }
}

