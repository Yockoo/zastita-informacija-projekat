﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CypherLibrary
{
    public static class SHA2
    {
        public static string GetHashcode(byte[] doc)
        {
            UInt32 a, b, c, d, e, f, g, h, t1, t2;
            UInt32 H0 = 0x6a09e667;
            UInt32 H1 = 0xbb67ae85;
            UInt32 H2 = 0x3c6ef372;
            UInt32 H3 = 0xa54ff53a;
            UInt32 H4 = 0x510e527f;
            UInt32 H5 = 0x9b05688c;
            UInt32 H6 = 0x1f83d9ab;
            UInt32 H7 = 0x5be0cd19;

            var procDoc = preProcessing(doc);
            byte[] data = new byte[procDoc.Length / 8];
            procDoc.CopyTo(data, 0);
            int t = data.Length / 64;
            for (int i = 0; i < t; i++)
            {
                UInt32[] w = new UInt32[64];
                for (int j = 0; j < 16; j++)
                    w[j] = data[64 * i + j];

                for (int j = 16; j < 64; j++)
                    w[j] = w[j - 16] + SIG0(w[j - 15]) + w[j - 7] + SIG1(w[j - 2]);

                a = H0;
                b = H1;
                c = H2;
                d = H3;
                e = H4;
                f = H5;
                g = H6;
                h = H7;
                //Rounds
                for (int j = 0; j < 64; j++)
                {
                    t1 = h + EP1(e) + CH(e, f, g) + K[j] + w[j];
                    t2 = EP0(a) + MAJ(a, b, c);
                    h = g;
                    g = f;
                    f = e;
                    e = d + t1;
                    d = c;
                    c = b;
                    b = a;
                    a = t1 + t2;
                }
                H0 += a;
                H1 += b;
                H2 += c;
                H3 += d;
                H4 += e;
                H5 += f;
                H6 += g;
                H7 += h;
            }
            StringBuilder sb = new StringBuilder(H0.ToString("X"));
            sb.Append(H1.ToString("X"));
            sb.Append(H2.ToString("X"));
            sb.Append(H3.ToString("X"));
            sb.Append(H4.ToString("X"));
            sb.Append(H5.ToString("X"));
            sb.Append(H6.ToString("X"));
            sb.Append(H7.ToString("X"));
            return sb.ToString();
        }
        private static BitArray preProcessing(byte[] data)
        {
            var bits = new BitArray(data);

            int modulo = (bits.Length % 512) + 1;
            int padding = modulo < 448 ? 448 - modulo : modulo - 448;
            bool[] paddedBits = new bool[bits.Length + padding + 64 + 1];
            bits.CopyTo(paddedBits, 0);
            paddedBits[bits.Length] = true;

            for (int i = bits.Length + 1; i < bits.Length + padding; i++)
            {
                paddedBits[i] = false;
            }

            UInt64 l = (UInt64)bits.Length;
            var lengthData = new BitArray(BitConverter.GetBytes(l));

            for (int i = bits.Length + padding + 1, j = 63; i < bits.Length + padding + 64; i++, j--)
            {
                paddedBits[i] = lengthData[j];
            }
            return new BitArray(paddedBits);
        }


        static uint ROTRIGHT(uint a, byte b)
        {
            return (((a) >> (b)) | ((a) << (32 - (b))));
        }

        static uint CH(uint x, uint y, uint z)
        {
            return (((x) & (y)) ^ (~(x) & (z)));
        }

        static uint MAJ(uint x, uint y, uint z)
        {
            return (((x) & (y)) ^ ((x) & (z)) ^ ((y) & (z)));
        }

        static uint EP0(uint x)
        {
            return (ROTRIGHT(x, 2) ^ ROTRIGHT(x, 13) ^ ROTRIGHT(x, 22));
        }

        static uint EP1(uint x)
        {
            return (ROTRIGHT(x, 6) ^ ROTRIGHT(x, 11) ^ ROTRIGHT(x, 25));
        }

        static uint SIG0(uint x)
        {
            return (ROTRIGHT(x, 7) ^ ROTRIGHT(x, 18) ^ ((x) >> 3));
        }

        static uint SIG1(uint x)
        {
            return (ROTRIGHT(x, 17) ^ ROTRIGHT(x, 19) ^ ((x) >> 10));
        }

        private static UInt32[] K = {
                0x428a2f98,0x71374491,0xb5c0fbcf,0xe9b5dba5,0x3956c25b,0x59f111f1,0x923f82a4,0xab1c5ed5,
                0xd807aa98,0x12835b01,0x243185be,0x550c7dc3,0x72be5d74,0x80deb1fe,0x9bdc06a7,0xc19bf174,
                0xe49b69c1,0xefbe4786,0x0fc19dc6,0x240ca1cc,0x2de92c6f,0x4a7484aa,0x5cb0a9dc,0x76f988da,
                0x983e5152,0xa831c66d,0xb00327c8,0xbf597fc7,0xc6e00bf3,0xd5a79147,0x06ca6351,0x14292967,
                0x27b70a85,0x2e1b2138,0x4d2c6dfc,0x53380d13,0x650a7354,0x766a0abb,0x81c2c92e,0x92722c85,
                0xa2bfe8a1,0xa81a664b,0xc24b8b70,0xc76c51a3,0xd192e819,0xd6990624,0xf40e3585,0x106aa070,
                0x19a4c116,0x1e376c08,0x2748774c,0x34b0bcb5,0x391c0cb3,0x4ed8aa4a,0x5b9cca4f,0x682e6ff3,
                0x748f82ee,0x78a5636f,0x84c87814,0x8cc70208,0x90befffa,0xa4506ceb,0xbef9a3f7,0xc67178f2
        };
    }
}
