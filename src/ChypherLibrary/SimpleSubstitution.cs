﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CypherLibrary
{
    public static class SimpleSubstitution
    {
        
        public static byte[] Decrypt(byte[] data, string key)
        {
            char[] cypherTable = new char[CYPHER_TABLE_LENGTH];
            cypherTable = generateChypherBasedOnKey(CYPHER_TABLE_LENGTH, key.GetHashCode());
        
            //data is byte array represents a string of UTF-8 characters
            string encryptedString = Encoding.UTF8.GetString(data);
            char[] chars = new char[encryptedString.Length];

            for (int i = 0; i < encryptedString.Length; i++)
            {
                if (encryptedString[i] == '=')
                {
                    chars[i] = '=';
                }
                else
                {
                    int base64indexOfChar = Array.IndexOf(cypherTable, encryptedString[i]);
                    chars[i] = Base64Letters[base64indexOfChar];
                }
            }
            return Convert.FromBase64CharArray(chars, 0, chars.Length);
        }

        public static byte[] Ecrypt(byte[] data, string key)
        {
            char[] cypherTable = new char[CYPHER_TABLE_LENGTH];
            cypherTable = generateChypherBasedOnKey(CYPHER_TABLE_LENGTH, key.GetHashCode());

            string dataString = Convert.ToBase64String(data);
            char[] chars = new char[dataString.Length];

            for (int i = 0; i < dataString.Length; i++)
            {
                if (dataString[i] == '=')
                    chars[i] = '=';
                else
                {
                    int base64indexOfChar = Array.IndexOf(Base64Letters, dataString[i]);
                    chars[i] = cypherTable[base64indexOfChar];
                }
            }
            // Convert data from normal string of utf-8 caracters to coresponding bytes
            string encryptedString = new string(chars);
            byte[] encryptedBytes = Encoding.UTF8.GetBytes(encryptedString);
            string base64EncryptedBytes = Convert.ToBase64String(encryptedBytes);

            return Convert.FromBase64String(base64EncryptedBytes);
        }

        private static char[] generateChypherBasedOnKey(int size, int key)
        {
            char[] exchanges = new char[size];
            Array.Copy(Base64Letters, exchanges, size);
            List<char> posibles = exchanges.ToList();
            var rand = new Random(key);
            for (int i = size - 1; i >= 0; i--)
            {
                int n = rand.Next(posibles.Count);
                exchanges[size - 1 - i] = posibles[n];
                posibles.RemoveAt(n);
            }
            return exchanges;
        }
        private const int CYPHER_TABLE_LENGTH = 64;
        private static readonly char[] Base64Letters = new[]
                                        {
                                              'A'
                                            , 'B'
                                            , 'C'
                                            , 'D'
                                            , 'E'
                                            , 'F'
                                            , 'G'
                                            , 'H'
                                            , 'I'
                                            , 'J'
                                            , 'K'
                                            , 'L'
                                            , 'M'
                                            , 'N'
                                            , 'O'
                                            , 'P'
                                            , 'Q'
                                            , 'R'
                                            , 'S'
                                            , 'T'
                                            , 'U'
                                            , 'V'
                                            , 'W'
                                            , 'X'
                                            , 'Y'
                                            , 'Z'
                                            , 'a'
                                            , 'b'
                                            , 'c'
                                            , 'd'
                                            , 'e'
                                            , 'f'
                                            , 'g'
                                            , 'h'
                                            , 'i'
                                            , 'j'
                                            , 'k'
                                            , 'l'
                                            , 'm'
                                            , 'n'
                                            , 'o'
                                            , 'p'
                                            , 'q'
                                            , 'r'
                                            , 's'
                                            , 't'
                                            , 'u'
                                            , 'v'
                                            , 'w'
                                            , 'x'
                                            , 'y'
                                            , 'z'
                                            , '0'
                                            , '1'
                                            , '2'
                                            , '3'
                                            , '4'
                                            , '5'
                                            , '6'
                                            , '7'
                                            , '8'
                                            , '9'
                                            , '+'
                                            , '/'
                                        };
    }
}
