﻿using ChypherLibrary;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CyphersProgram
{
    class Program
    {
        static void Main(string[] args)
        {
            //string key = "passnemazasatadaujedebreeeee";
            byte[] file1 = File.ReadAllBytes("test2.docx");
            byte[] file2 = File.ReadAllBytes("test.txt");

            ////byte[] encryp1=XXTEA.Ecrypt(file1, key);
            ////byte[] dec1 = XXTEA.Decrypt(encryp1, key);


            //string pubKey = Knapsack.GetPublicKey(key);
            //byte[] enc = Knapsack.Encrypt(file1, pubKey);
            //byte[] dec = Knapsack.Decrypt(enc, key);

            //byte[] enc1 = Knapsack.Encrypt(file2, pubKey);
            //byte[] dec1 = Knapsack.Decrypt(enc1, key);


            //if (ByteArrayCompare(file1, dec))
            //    Console.WriteLine("Succes");

            //if (ByteArrayCompare(file2, dec1))
            //    Console.WriteLine("Succes");

            string r = SHA2.GetHashcode(file1);
            string r1 = SHA2.GetHashcode(file1);
            Console.WriteLine(r); Console.WriteLine(r1);

            string r2 = SHA2.GetHashcode(file2);
            string r21 = SHA2.GetHashcode(file2);
            Console.WriteLine(r2); Console.WriteLine(r21);
            Console.ReadKey();

        }
        static bool ByteArrayCompare(byte[] a1, byte[] a2)
        {
            if (a1.Length != a2.Length) {
                Console.WriteLine($"File {a1.Length} a dec {a2.Length}");
                return false;
            }
            for (int i = 0; i < a1.Length; i++)
                if (a1[i] != a2[i])
                {
                    Console.WriteLine($"Greska na {i} bytu od ukupno {a1.Length}");
                    return false;
                }
            return true;
        }
    }
}
